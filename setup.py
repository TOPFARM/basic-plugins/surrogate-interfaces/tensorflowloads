from setuptools import setup

setup(
    name='TensorFlowLoads',
    version='0.0.1',
    description='Load constraint plugin for TopFarm with TensorFlow for load surrogates',
    install_requires=[
	  'topfarm',
	  'tensorflow>=2.0.0-beta1'],
#    package_dir={'': ''},
    dependency_links=['https://gitlab.windenergy.dtu.dk/TOPFARM/TopFarm2.git'],
    packages=['tensorflowloads'],
    license='All rights reserved',
    entry_points={'topfarm.plugins': 'tensorflowloads = tensorflowloads'},
)
