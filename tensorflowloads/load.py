"""
Provides support to predict the output and its gradient using tensorflow
models. At the moment only tensorflow.keras.Sequential models are supported.
"""


# %% Dictionaries for openturns and tensorflow

# Dictionary used by predict_output() to call the correct function for
# each model type.
_switch_output_update = {}

# Dictionary used by predict_gradient() to call the correct function for
# each model type.
_switch_gradient_update = {}


# %% Functions related to tensorflow neural networks.

try:
    import tensorflow as tf

    def _predict_output_tf_Sequential(model, input):
        """
        Predict output function for tensorflow.keras Sequential objects.
        """
        output = model.predict(input)
        return output

    def _predict_gradient_tf_Sequential(model, input):
        """
        Predict gradient function for tensorflow.keras Sequential objects.
        """
        inp_ = tf.constant(input)
        with tf.GradientTape() as tape:
            tape.watch(inp_)
            out_ = model(inp_)
        gradient = tape.gradient(out_, inp_).numpy()
        return gradient

    _switch_output_update[tf.keras.Sequential] = _predict_output_tf_Sequential

    _switch_gradient_update[tf.keras.Sequential] = _predict_gradient_tf_Sequential

except ModuleNotFoundError:
    pass


def update_switch():
    return _switch_output_update, _switch_gradient_update
