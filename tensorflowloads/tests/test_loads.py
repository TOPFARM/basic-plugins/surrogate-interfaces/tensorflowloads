import numpy as np
import tensorflow as tf
# from tensorflow.estimator import DNNRegressor
from tensorflow import keras
from tensorflow.keras import layers
import os
import random
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from topfarm.constraint_components.load import (
        predict_output, predict_gradient, compute_error)


def test_tensorflow_output():
    """
    Test predict_output() when model is a tensorflow.keras.Sequential.
    """

    os.environ['PYTHONHASHSEED'] = str(0)
    random.seed(1)
    np.random.seed(2)
    tf.random.set_seed(3)

    input = np.linspace(-5, 5, 11).reshape(-1, 1)
    output = 2.0 * input + 3.0

    model = keras.Sequential([
        layers.Dense(1, activation=None, use_bias=True),
    ])

    model.compile(loss='mean_squared_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=1e-1),
                  metrics=['mean_absolute_percentage_error', ])

    history = model.fit(input,
                        output,
                        epochs=200,
                        validation_split=0.2,
                        batch_size=len(input),
                        verbose=0,)

    predicted_output, _ = predict_output(model, input)

    assert np.allclose(output, predicted_output, atol=1e-3)

    # model.weights

#    import matplotlib.pyplot as plt
#    fig, ax = plt.subplots()
#    ax.scatter(input, output)
#    ax.scatter(input, predicted_output)


# To be finished.
def p_test_tensorflow_gradient_1():
    """
    Test predict_gradient() when model is a tensorflow.keras.Sequential.
    """

    os.environ['PYTHONHASHSEED'] = str(0)
    random.seed(1)
    np.random.seed(2)
    tf.random.set_seed(3)

    input = np.linspace(-np.pi, +np.pi, 100).reshape(-1, 1)
    output = np.sin(input)

    validation_input = np.linspace(-5*np.pi, +5*np.pi, 1000).reshape(-1, 1)
    validation_output = np.sin(validation_input)

    model = keras.Sequential([
        layers.Dense(1, activation=None, use_bias=True,
                     kernel_constraint=tf.keras.constraints.UnitNorm()),
        layers.Dense(1, activation=tf.nn.tanh, use_bias=False),
        layers.Dense(1, activation=tf.nn.tanh, use_bias=True,
                     bias_initializer=tf.initializers.Constant(value=-np.pi),),
        layers.Dense(1, activation=None),
    ])

    model.compile(loss='mean_squared_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))

    history = model.fit(input,
                        output,
                        epochs=4000,
                        validation_split=0.2,
                        batch_size=len(input),
                        verbose=0,)

    predicted_input = (validation_input + np.pi) % (2 * np.pi) - np.pi
    predicted_output, _ = predict_output(model, predicted_input)

#    assert np.allclose(output, predicted_output, atol=1e-3)

    predicted_gradient = predict_gradient(model, predicted_input)

#    import matplotlib.pyplot as plt
#    fig, ax = plt.subplots()
#    ax.scatter(validation_input, validation_output, label='Validation')
#    ax.plot(validation_input, predicted_output, label='Prediction')
#    ax.scatter(input, output, label='Training')
#    ax.plot(validation_input, predicted_gradient, label='Predicted Gradient')
#    ax.legend()
#
#    # Plot history.
#    hist_ = pd.DataFrame(history.history)
#    fig, ax = plt.subplots()
#    ax.plot(hist_['loss'], 'b', label='loss')
#    ax.plot(hist_['val_loss'], 'r', label='val_loss')
#    ax.legend()
#    ax.set_yscale('log')


def test_tensorflow_gradient_2():
    """
    Test predict_gradient() when model is a tensorflow.keras.Sequential.
    """

    os.environ['PYTHONHASHSEED'] = str(0)
    random.seed(1)
    np.random.seed(2)
    tf.random.set_seed(3)

    input = np.linspace(-5.0, +10.0, 100).reshape(-1, 1)
    output = np.tanh(input) - 2.0 * np.tanh(input)
    exact_gradient = 1.0 - np.tanh(input) ** 2.0 - 2.0 * (1.0 - np.tanh(input) ** 2.0)

    model = keras.Sequential([
        layers.Dense(1, activation=tf.nn.tanh),
        layers.Dense(1, activation=tf.nn.tanh),
    ])

    model.compile(loss='mean_squared_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=1e-1))

    history = model.fit(input,
                        output,
                        epochs=5000,
                        validation_split=0.2,
                        batch_size=len(input),
                        verbose=0,)

    predicted_output, _ = predict_output(model, input)
    predicted_gradient, _ = predict_gradient(model, input)

    assert np.allclose(output, predicted_output, atol=1e-2)
    assert np.allclose(exact_gradient, predicted_gradient, atol=0.05)

#    import matplotlib.pyplot as plt
#    fig, ax = plt.subplots()
#    ax.scatter(input, output, label='True output')
#    ax.plot(input, predicted_output, label='Predicted output')
#    ax.scatter(input, exact_gradient, label='True gradient')
#    ax.plot(input, predicted_gradient, label='Predicted Gradient')
#    ax.legend()

#    # Plot history.
#    hist_ = pd.DataFrame(history.history)
#    fig, ax = plt.subplots()
#    ax.plot(hist_['loss'], 'b', label='loss')
#    ax.plot(hist_['val_loss'], 'r', label='val_loss')
#    ax.legend()
#    ax.set_yscale('log')


def test_tensorflow_gradient_3():
    """
    Test predict_gradient() when model is a tensorflow.keras.Sequential.
    """

    os.environ['PYTHONHASHSEED'] = str(0)
    random.seed(1)
    np.random.seed(2)
    tf.random.set_seed(3)

    input = 4 * np.random.rand(500, 2) - 2
    output = (np.tanh(input[:, 0]) - 2.0 * np.tanh(input[:, 1])).reshape(-1, 1)
    exact_gradient = np.zeros_like(input)
    exact_gradient[:, 0] = 1 - np.tanh(input[:, 0]) ** 2
    exact_gradient[:, 1] = - 2.0 * (1 - np.tanh(input[:, 1]) ** 2)

    model = keras.Sequential([
        layers.Dense(2, activation=tf.nn.tanh, input_shape=[2]),
        layers.Dense(1, activation=None),
    ])

    model.compile(loss='mean_squared_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))

    history = model.fit(input,
                        output,
                        epochs=9000,
                        validation_split=0.2,
                        batch_size=len(input),
                        verbose=0,)

    predicted_output, _ = predict_output(model, input)
    predicted_gradient, _ = predict_gradient(model, input)

    assert np.allclose(output, predicted_output, atol=1e-2)
    assert np.allclose(exact_gradient, predicted_gradient, atol=1e-2)

#    x = np.linspace(np.min(input[:,0]), np.max(input[:,0]), 50)
#    y = np.linspace(np.min(input[:,0]), np.max(input[:,0]), 50)
#    X, Y = np.meshgrid(x, y)
#    predicted_input = np.column_stack((X.ravel(), Y.ravel()))
#    predicted_output, _ = predict_output(model, predicted_input)
#    predicted_output = predicted_output.reshape(len(x), len(y))
#    predicted_gradient, _ = predict_gradient(model, predicted_input)
#    predicted_gradient_0 = predicted_gradient[:,0].reshape(len(x), len(y))
#    predicted_gradient_1 = predicted_gradient[:,1].reshape(len(x), len(y))
#    
#    from mpl_toolkits.mplot3d import Axes3D
#    import matplotlib.pyplot as plt
#    fig = plt.figure(num='Output')
#    ax = fig.add_subplot(111, projection='3d')
#    ax.scatter(input[:,0], input[:,1], output)
#    ax.plot_surface(X, Y, predicted_output)
#
#    fig = plt.figure(num='Gradient')
#    ax = fig.add_subplot(1, 2, 1, projection='3d')
#    ax.scatter(input[:,0], input[:,1], exact_gradient[:,0])
#    ax.plot_surface(X, Y, predicted_gradient_0)
#    ax = fig.add_subplot(1, 2, 2, projection='3d')
#    ax.scatter(input[:,0], input[:,1], exact_gradient[:,1])
#    ax.plot_surface(X, Y, predicted_gradient_1)
#
#    # Plot history.
#    hist_ = pd.DataFrame(history.history)
#    fig, ax = plt.subplots()
#    ax.plot(hist_['loss'], 'b', label='loss')
#    ax.plot(hist_['val_loss'], 'r', label='val_loss')
#    ax.legend()
#    ax.set_yscale('log')

def p_test_tensorflow_gradient_4():
    """
    Test predict_gradient() when model is a tensorflow.keras.Sequential.
    In this case we use both input and output scalers.
    """

    os.environ['PYTHONHASHSEED'] = str(0)
    random.seed(1)
    np.random.seed(2)
    tf.random.set_seed(3)

    input = 200.0 * np.random.rand(5000, 2) - 100.0
    output = (0.01 * input[:, 0] ** 2.0
              + 0.02 * input[:, 1] ** 2.0
              + 5000.0 * np.cos(2.0 * np.pi * 0.01 * input[:, 0])
              + 3000.0 * np.sin(2.0 * np.pi * 0.02 * input[:, 1])).reshape(-1, 1)

    exact_gradient = np.zeros_like(input)
    exact_gradient[:, 0] = (2 * 0.01 * input[:, 0]
                            - 5000.0 * 2.0 * np.pi * 0.01 * np.sin(2.0 * np.pi * 0.01 * input[:, 0]))
    exact_gradient[:, 1] = (2 * 0.02 * input[:, 1]
                            + 3000.0 * 2.0 * np.pi * 0.02 * np.cos(2.0 * np.pi * 0.02 * input[:, 1]))

    input_scaler = MinMaxScaler()
    input_scaled = input_scaler.fit_transform(input)
    output_scaler = MinMaxScaler()
    output_scaled = output_scaler.fit_transform(output)

    model = keras.Sequential([
        layers.Dense(50, activation=tf.nn.tanh, input_shape=[2]),
        layers.Dense(50, activation=tf.nn.tanh, input_shape=[2]),
        layers.Dense(1, activation=None),
    ])

    model.compile(loss='mean_squared_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=5e-5))

    history = model.fit(input_scaled,
                        output_scaled,
                        epochs=69900,
                        validation_split=0.1,
                        batch_size=len(input),
                        verbose=0,)

    predicted_output, _ = predict_output(model,
                                         input,
                                         input_scaler=input_scaler,
                                         output_scaler=output_scaler)
    predicted_gradient, _ = predict_gradient(model,
                                             input,
                                             input_scaler=input_scaler,
                                             output_scaler=output_scaler)

    abs_err_output, rel_err_output = compute_error(output, predicted_output)
    rel_err_output *= 100

    abs_err_gradient, rel_err_gradient = compute_error(exact_gradient, predicted_gradient)
    rel_err_gradient *= 100

    print('Output rel. err mean {:.4} %    2*std {:.4} %'.format(rel_err_output.mean(), 2.0*rel_err_output.std()))
    print('Gradient rel. err mean {:.4} %    2*std {:.4} %'.format(rel_err_gradient.mean(), 2.0*rel_err_gradient.std()))
#
#    assert np.allclose(output, predicted_output, rtol=1e-2)
#    assert np.allclose(exact_gradient, predicted_gradient, rtol=1e-2)

    # Plot history.
    import matplotlib.pyplot as plt
    hist_ = pd.DataFrame(history.history)
    fig, ax = plt.subplots()
    ax.plot(hist_['loss'], 'b', label='loss')
    ax.plot(hist_['val_loss'], 'r', label='val_loss')
    ax.legend()
    ax.set_yscale('log')
